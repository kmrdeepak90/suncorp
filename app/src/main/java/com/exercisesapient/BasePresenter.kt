package com.exercisesapient

abstract class BasePresenter<out V : BaseView >(protected val view: V){

    //Method to notify Presenter about View Creation
    abstract fun onViewCreated()

    //Method to Notify Presenter about view state
    abstract fun onViewDestroyed()
}