package com.exercisesapient

import android.content.Context

interface BaseView {

    //Provide Context incase presenter needs android api's
    fun getContext() : Context

    //Method to show error in View
    fun showError(error : String)

    //Show the Loading Indication
    fun showLoading()

    //Hide Loding Indicator
    fun hideLoading()

}