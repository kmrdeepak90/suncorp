package com.exercisesapient

import android.app.Application
import com.exercisesapient.di.AppComponent
import com.exercisesapient.di.DBModule
import com.exercisesapient.di.DaggerAppComponent

class MyApplication : Application() {


    lateinit var component: AppComponent

    override fun onCreate() {



        super.onCreate()
        createComponent()
    }

    protected open fun createComponent() {
        component = DaggerAppComponent.builder().dBModule(DBModule(this)).build()

        component.inject(this)


    }

    open fun getApplicationComponent(): AppComponent {
        return component
    }


//    companion object {
//        var database: AppDatabase? = null
//    }

//    override fun onCreate() {
//        super.onCreate()
//        database =  Room.databaseBuilder(this, AppDatabase::class.java, DATABASE_NAME).build()
//    }
}