package com.exercisesapient.di

import com.exercisesapient.MyApplication
import com.exercisesapient.transaction.TransactionActivity
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = arrayOf(DBModule::class))
interface AppComponent {

    fun inject(myApplication: MyApplication)

    fun inject (activity : TransactionActivity)
}