package com.exercisesapient.di

import android.arch.persistence.room.Room
import android.content.Context
import com.exercisesapient.database.AppDatabase
import com.exercisesapient.database.TransactionDao
import com.exercisesapient.network.ApiHelper
import com.exercisesapient.util.API_BASE_URL
import com.exercisesapient.util.DATABASE_NAME
import dagger.Module
import dagger.Provides
import dagger.Reusable
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
class DBModule (private val context: Context){


    @Provides
    @Singleton
    internal fun provideContext(): Context = context

    @Provides
    @Singleton
    internal fun provideAppDatabase(): AppDatabase =
       Room.databaseBuilder(context, AppDatabase::class.java, DATABASE_NAME).build()

    @Provides
    @Singleton
    internal fun provideTransactionDao(appDatabase: AppDatabase): TransactionDao =
            appDatabase.transactionDao()


    /*
    * Provides the Post service implementation.
    * @param retrofit the Retrofit object used to instantiate the service
    * @return the Post service implementation.
    */
    @Provides
    @Reusable
    internal fun providePostApi(retrofit: Retrofit): ApiHelper {
        return retrofit.create(ApiHelper::class.java)
    }

    /**
     * Provides the Retrofit object.
     * @return the Retrofit object
     */
    @Provides
    @Reusable
    internal fun provideRetrofitInterface(client: OkHttpClient): Retrofit {
        return Retrofit.Builder().baseUrl(API_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(client)
                .build()
    }
}