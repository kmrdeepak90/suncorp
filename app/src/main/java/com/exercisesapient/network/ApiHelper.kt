package com.exercisesapient.network

import com.exercisesapient.transaction.Transaction
import com.exercisesapient.util.API_BASE_URL
import dagger.Module
import io.reactivex.Observable
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET


interface ApiHelper {

    /**
     * Get the list of the transactions from the API
     */
    @GET("/transactions")
    fun getTransactions(): Observable<List<Transaction>>


    companion object Factory {
        fun create(): ApiHelper {
            val retrofit = retrofit2.Retrofit.Builder()
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .baseUrl(API_BASE_URL)
                    .build()

            return retrofit.create(ApiHelper::class.java)
        }
    }

}