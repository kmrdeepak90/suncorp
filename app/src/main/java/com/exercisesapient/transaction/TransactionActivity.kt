package com.exercisesapient.transaction

import android.content.Context
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import com.exercisesapient.MyApplication
import com.exercisesapient.R
import com.exercisesapient.databinding.ActivityTransactionBinding
import com.exercisesapient.di.DBModule
import com.exercisesapient.di.DaggerAppComponent
import kotlinx.android.synthetic.main.activity_transaction.*

class TransactionActivity : AppCompatActivity(), TransactionView {


    private  var transactions: ArrayList<Transaction> = ArrayList()

    private var presenter = TransactionPresenter(this)
    private val adapter = TransactionAdapter(transactions)

    private lateinit var binding : ActivityTransactionBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        DaggerAppComponent.builder().dBModule(DBModule(this)).build().inject(this)
        super.onCreate(savedInstanceState)

        binding = DataBindingUtil.setContentView(this, R.layout.activity_transaction)

        showLoading()

        // elements are laid out.
        binding.transactionRecyclerView.layoutManager = LinearLayoutManager(this)

        // Set CustomAdapter as the adapter for RecyclerView.
        binding.transactionRecyclerView.adapter = adapter

        presenter.onViewCreated()
        presenter.fetchTransactionHistory()
    }

    override fun getContext(): Context {
        return this;
    }

    override fun showError(error: String) {
        TODO("not implemented")
        //Add code to handle errors
    }

    override fun showLoading() {
        view_progressbar.visibility = View.VISIBLE
    }

    override fun hideLoading() {
        view_progressbar.visibility = View.GONE
    }

    override fun showTransactionHistory(transactions: List<Transaction>) {
        hideLoading()
        this.transactions.clear()
        this.transactions.addAll(transactions)
        val balance: Float = transactions.map { it.amount }.sum()
        binding.balanceText.text = balance.toString()
        binding.transactionRecyclerView.adapter?.notifyDataSetChanged()
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.onViewDestroyed()
    }
}