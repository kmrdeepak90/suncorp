package com.exercisesapient.transaction

import com.exercisesapient.BasePresenter
import com.exercisesapient.MyApplication
import com.exercisesapient.database.AppDatabase
import com.exercisesapient.database.TransactionDao
import com.exercisesapient.network.ApiHelper
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class TransactionPresenter(view: TransactionView) : BasePresenter<TransactionView>(view) {

    private var mDisposable: Disposable? = null

    @Inject lateinit var mTransactionDao: TransactionDao

    override fun onViewDestroyed() {
        mDisposable?.dispose()
    }


    override fun onViewCreated() {}

    fun fetchTransactionHistory() {
        mDisposable = Observable.fromCallable({
            mTransactionDao?.getTransactionHistory()
        }).flatMap { transactionList ->
            if (
                    transactionList.isNotEmpty()) Observable.just(transactionList) else saveResponse()
        }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .doOnTerminate { view.hideLoading() }.subscribe(this::handleResponse, this::handleError)
    }

    private fun saveResponse(): Observable<List<Transaction>>? {

        return ApiHelper.create().getTransactions()
                .flatMap { transactionList ->
                    Observable.fromCallable({
                        mTransactionDao?.insertTransactions(transactionList);transactionList
                    })
                }

    }

    private fun handleResponse(transactions: List<Transaction>) {
        view.showTransactionHistory(transactions.sortedByDescending { it.effectiveDate })
    }

    private fun handleError(error: Throwable) {}

}