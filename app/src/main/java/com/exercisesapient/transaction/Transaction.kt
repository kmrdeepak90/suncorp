package com.exercisesapient.transaction

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity(tableName = "Transaction_Data")
public data class Transaction (@ColumnInfo(name = "id")
                               @PrimaryKey(autoGenerate = true) val id: Int, @ColumnInfo var amount : Float,
                               @ColumnInfo var description : String,
                               @ColumnInfo var effectiveDate : String )