package com.exercisesapient.transaction

import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.exercisesapient.R
import com.exercisesapient.databinding.ViewTransactionBinding

class TransactionAdapter(private val transactions: ArrayList<Transaction>) :
        RecyclerView.Adapter<TransactionAdapter.ViewHolder>() {

    /**
     * Provide a reference to the type of views that you are using (custom ViewHolder)
     */
    class ViewHolder(val binding: ViewTransactionBinding) : RecyclerView.ViewHolder(binding.root) {

        /**
         * Bind a transaction into the view
         */
        fun bind(transaction: Transaction) {
            binding.transaction = transaction
            binding.executePendingBindings()
        }
    }


    // Create new views (invoked by the layout manager)
    override fun onCreateViewHolder(parent: ViewGroup,
                                    viewType: Int): TransactionAdapter.ViewHolder {
        // create a new view

        val layoutInflater = LayoutInflater.from(parent.context)
        val binding: ViewTransactionBinding = DataBindingUtil.inflate(layoutInflater,
                R.layout.view_transaction, parent, false)
        return ViewHolder(binding)
    }

    // Replace the contents of a view (invoked by the layout manager)
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(transactions[position])


    }

    // Return the size of your dataset (invoked by the layout manager)
    override fun getItemCount() = transactions.size
}