package com.exercisesapient.transaction

import com.exercisesapient.BaseView

interface TransactionView : BaseView {
    fun showTransactionHistory( transactions : List<Transaction>)
}