package com.exercisesapient.database

import android.arch.persistence.room.RoomDatabase
import android.arch.persistence.room.Database
import com.exercisesapient.transaction.Transaction


@Database(entities = arrayOf(Transaction::class), version = 1)
abstract class AppDatabase : RoomDatabase() {
    abstract fun transactionDao(): TransactionDao
}
