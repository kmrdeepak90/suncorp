package com.exercisesapient.database

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.Query
import com.exercisesapient.transaction.Transaction

@Dao
interface TransactionDao {

    @Query("SELECT * FROM Transaction_Data ")
    fun getTransactionHistory(): List<Transaction>

    @Insert
    fun insertTransactions(transactions : List<Transaction>)

}