## Suncorp Exercise

Assignment to demonstrate Android app implemented with MVP Architecture, Kotlin, Rx Java, Rx Android, Room Database .

---

## SourceCode

### 1. Room Database


         @Entity(tableName = "Transaction_Data")
         public data class Transaction (@ColumnInfo(name = "id")
                                       @PrimaryKey(autoGenerate = true) val id: Int,
                                       @ColumnInfo var amount : Float,
                                       @ColumnInfo var description : String,
                                       @ColumnInfo var effectiveDate : String )


### 2. Dao


          @Dao
          public interface TransactionDao {

              @Query("SELECT * FROM Transaction_Data ")
              fun getTransactionHistory(): List<Transaction>

              @Insert
              fun insertTransactions(transactions : List<Transaction>)

          }


## 3. Adapter with DataBinding

        class TransactionAdapter (private val transactions: ArrayList<Transaction>) :
                RecyclerView.Adapter<TransactionAdapter.ViewHolder>()  {

            /**
             * Provide a reference to the type of views that you are using (custom ViewHolder)
             */
            class ViewHolder(val binding: ViewTransactionBinding) : RecyclerView.ViewHolder(binding.root) {
                /**
                 * Bind a transaction into the view
                 */
                fun bind(transaction: Transaction) {
                    binding.transaction = transaction
                    binding.executePendingBindings()
                }
            }

            // Create new views (invoked by the layout manager)
            override fun onCreateViewHolder(parent: ViewGroup,
                                            viewType: Int): TransactionAdapter.ViewHolder {
                // create a new view
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding: ViewTransactionBinding = DataBindingUtil.inflate(layoutInflater,
                        R.layout.view_transaction, parent, false)
                return ViewHolder(binding)
            }

            // Replace the contents of a view (invoked by the layout manager)
            override fun onBindViewHolder(holder: ViewHolder, position: Int) {
                holder.bind(transactions[position])
            }

            // Return the size of your dataset (invoked by the layout manager)
            override fun getItemCount() = transactions.size
        }

### 4. TransactionPresenter

            class TransactionPresenter(view: TransactionView) : BasePresenter<TransactionView>(view) {

                private var mDisposable: Disposable? = null

                override fun onViewDestroyed() {
                    mDisposable?.dispose()
                }


                override fun onViewCreated() {}

                fun fetchTransactionHistory() {
                    mDisposable = Observable.fromCallable({ MyApplication.database?.transactionDao()
                            ?.getTransactionHistory() }).flatMap { transactionList -> if (
                            transactionList.isNotEmpty()) Observable.just(transactionList) else saveResponse() }
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribeOn(Schedulers.io())
                            .doOnTerminate { view.hideLoading() }.subscribe(this::handleResponse, this::handleError)
                }

                private fun saveResponse(): Observable<List<Transaction>>? {
                    return ApiHelper.create().getTransactions()
                            .flatMap { transactionList -> Observable.fromCallable({
                                MyApplication.database?.transactionDao()
                                        ?.insertTransactions(transactionList);transactionList }) }
                }

                private fun handleResponse(transactions: List<Transaction>) {
                    view.showTransactionHistory(transactions.sortedByDescending { it.effectiveDate })
                }

                private fun handleError(error: Throwable) {}

            }